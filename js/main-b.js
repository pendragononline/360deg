var viewer = new Marzipano.Viewer(document.getElementById('pano'));
var deviceOrientationControlMethod = new DeviceOrientationControlMethod();
var controls = viewer.controls();
controls.registerMethod('deviceOrientation', deviceOrientationControlMethod);
var geometry = new Marzipano.EquirectGeometry([{
    width: 4096
}]);
var limiter = Marzipano.RectilinearView.limit.traditional(2048, 100 * Math.PI / 180);
var view = new Marzipano.RectilinearView({
    yaw: Math.PI
}, limiter);
var source = Marzipano.ImageUrlSource.fromString(
    "img/img.jpg"
);
// Create scene.
var scene = viewer.createScene({
    source: source,
    geometry: geometry,
    view: view,
    pinFirstLevel: true
});
// initiates view
scene.switchTo();



//HOTSPOTS - INFO BOXES

//Bandstand
scene.hotspotContainer().createHotspot(document.querySelector("#reveal1"), {
    yaw: -0.2,
    pitch: -0.2
});

//Oulton Broad
scene.hotspotContainer().createHotspot(document.querySelector("#reveal2"), {
    yaw: 2,
    pitch: 0.1
});

//Malt Houses
scene.hotspotContainer().createHotspot(document.querySelector("#reveal3"), {
    yaw: 3.4,
    pitch: 0.05
});

//Harbour
scene.hotspotContainer().createHotspot(document.querySelector("#reveal4"), {
    yaw: 5,
    pitch: -0.2
});




// Set up control for enabling/disabling device orientation.
var enabled = false;
var toggleElement = document.getElementById('toggleDeviceOrientation');

function enable() {
    deviceOrientationControlMethod.getPitch(function (err, pitch) {
        if (!err) {
            view.setPitch(pitch);
        }
    });
    controls.enableMethod('deviceOrientation');
    enabled = true;
    toggleElement.className = 'enabled';
}

function disable() {
    controls.disableMethod('deviceOrientation');
    enabled = false;
    toggleElement.className = '';
}

function toggle() {
    if (enabled) {
        disable();
    } else {
        enable();
    }
}
toggleElement.addEventListener('click', toggle);

